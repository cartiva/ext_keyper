# encoding=utf-8
"""
deployed 10/31/2018 on 10.130.196.22
runs every 5 minutes all day long, scheduled task: Real Time/pg_keyper_scrape
updates keys.ext_keyper (postgresql)
required to expose key location in the new vehicle pic loader
"""
import csv
import db_cnx
import ops
import time

file_name = 'files/ext_keyper.csv'
try:
    with db_cnx.keyper() as keyper_con:
        with keyper_con.cursor() as keyper_cur:
            sql = """
                select a.asset_name, 
                  convert(nvarchar(MAX), dateadd(hour, -5, a.transaction_date), 22) as transaction_date,
                  a.message, a.asset_status, 
                  convert(nvarchar(MAX), b.created_date, 101) as created_date,
                  coalesce(substring(c.name, charindex('-', c.name) + 1, 12), 'WTF-no cabinet') as cabinet,
                  d.first_name, d.last_name, coalesce(e.name, 'none') as issue_reason
                from dbo.asset_transactions a
                inner join dbo.asset b on a.asset_id = b.asset_id
                inner join dbo.system c on a.system_id = c.system_id
                inner join dbo.[user] d on a.user_id = d.user_id
                left join dbo.issue_reason e on a.issue_reason_id = e.issue_reason_id 
                where a.asset_name is not null
                  and b.deleted = 0                    
            """
            keyper_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(keyper_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate keys.ext_keyper_tmp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy keys.ext_keyper_tmp from stdin with csv encoding 'latin-1 '""", io)
            sql = "select keys.update_ext_keyper()"
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error('keyper update: ext_keyper.py', time.strftime("%m/%d/%Y"), error)
    print(error)
finally:
    pass